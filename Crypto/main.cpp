#include <iostream>
#include <vector>
#include <cstdint>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

using namespace std;

unsigned int osszeg(vector<uint_fast32_t>& kockak, vector<int>& indexVector, uint_fast32_t N){
    unsigned int sumOfValues = 0;
    for(int i : indexVector)
        sumOfValues += kockak[i]%N;
    return (sumOfValues % N);
}

void koviIndexeles(vector<int>& indexVector, uint_fast32_t N){
    for(int i = 0; i < N; i++){
        if(indexVector[N-1-i] != N*2-1-i){
            indexVector[N-1-i]++;
            for(int j = 1; j <= i; j++){
                indexVector[N-1-i+j] = indexVector[N-1-i] + j;
            }
            return;
        }
    }
}

void indexKiiras(vector<int>& indexVector){
    for(int i : indexVector)
        cout << i << ", ";
    cout << endl;
}

vector<uint_fast32_t> select_cubes( vector<uint_fast32_t>&& kockak,  uint_fast32_t N){
    // TODO return 'count' count number from 'cubes'
    // whose sum can be divided by 'divisor' with zero remainder
    vector<uint_fast32_t > kivalasztottKockak(0);
    vector<int> indexVector(N);
    for(int i = 0; i < N; i++){
        indexVector[i] = i;
    }
    /*indexKiiras(indexVector);
    for(int i : kockak)
        cout << i << ", ";
    cout << endl;*/

    while(osszeg(kockak, indexVector, N) != 0){
        koviIndexeles(indexVector, N);
        //indexKiiras(indexVector);
    }

    if(osszeg(kockak, indexVector, N) == 0){
        for(int i : indexVector)
            kivalasztottKockak.push_back(kockak[i]);
    }

    return kivalasztottKockak;
}



int main()
{
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);
    std::uint_fast32_t boxA, boxB, boxC;
    std::cin >> boxA >> boxB >> boxC;
    auto N = boxA * boxB * boxC;
    std::vector<std::uint_fast32_t> cubes(N * 2);
    for (auto& cube : cubes) std::cin >> cube;
    if (auto&& result = select_cubes(std::move(cubes), N); result.size() == N)
        for (auto& cube : result) std::cout << cube << " ";
    else
        std::cout << "****";
    std::endl(std::cout);
}

/*
int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    uint_fast32_t boxA, boxB, boxC;
    vector<uint_fast32_t> cubes;
    srand (time(NULL));
    boxA = 10;
    boxB = 10;
    boxC = 10;
    for(unsigned int i =0; i< (2*boxA*boxB*boxC); i++){
        cubes.push_back(rand() % 1000);
    }
    //auto N =  boxA * boxB * boxC;
    auto N = 4;
    vector<uint_fast32_t> cubes2 = {3,3,3,2,2,2,1,1};

    if (auto && result = select_cubes(move(cubes2), N); result.size() == N)
        for (auto& cube : result) cout << cube << " ";
    else
        cout << "****";
    endl(cout);
}*/
=======
#include <map>
#include <bitset>
#include <vector>

#define M 8

using namespace std;

enum muveletTipus{ADD, XOR};

//const string EREDETI ="SZIASZTOK\nBOLDOG KARACSONYT\nMARKETING CSAPAT";
//const string KODOLT = "HARZHAGLP\nYLOWLT PZIZXHLMBG\nNZIPVGRMT XHZKZG";

void readData(short& N, std::map<short, short> &charMap){

    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    std::uint_fast32_t steps;
    (std::cin >> steps).ignore(4, '\n');

    std::string input, output, tmp;

    while(std::getline(std::cin, tmp) && !tmp.empty())
        input += std::move(tmp);

    while(std::getline(std::cin, tmp) && !tmp.empty())
        output += std::move(tmp);

    N = steps;
    //input = EREDETI;
    //output = KODOLT;

    //Átalakítja kódtáblává a be- és kimenetet
    for(int i = 0; i < (int)input.size(); i++){
        if(input[i] != ' ' && input[i] !='\n')
            charMap[(short)input[i]] = (short)output[i];
    }

}

void readData2(short& N, std::map<short, short> &charMap){
    cin >> N >> ws;
    string sor = "";
    string input="", output="";
    int sordb = 0;
    cout << N << endl;
    while(sor != "/n"){
        getline(cin, sor);
        input += sor;
        sordb++;
    }
    cout << "Input: " << input;
    for(int i = 0; i < sordb-1; i++){
        getline(cin, sor);
        output += sor;
    }

    cout <<"output: " << output;

    //Átalakítja kódtáblává a be- és kimenetet
    for(int i = 0; i < (int)input.size(); i++){
        if(input[i] != ' ' && input[i] !='\n')
            charMap[(short)input[i]] = (short)output[i];
    }

}


void writeOutData(const short& N, const std::map<short, short> &charMap){
    std::cout << "Num of operation: " << N << '\n';
    for(pair<short,short> P : charMap){
        std::cout << P.first << " -> " << P.second << '\n';
    }
}


void kovetkezoLehetoseg(vector<short>& muveletek){
    if(muveletek.size() == 0){
        muveletek = {0};
        return;
    }

    for(int i = muveletek.size()-1; i >= 0; i--){
        muveletek[i]++;

        if(muveletek[i] != 256)
            return;

        muveletek[i] = 0;
        if(i == 0){
            muveletek.push_back(0);
            muveletek[0] = 1;
        }
    }
}

bool joMegoldasE(const std::map<short, short> &charMap, const vector<short>& muveletek, const muveletTipus kezdoMuvelet){
    for(pair<bitset<M>, bitset<M>> P : charMap){
        bitset<M> left = P.first;
        for(int j = 0; j < muveletek.size(); j++){
            bitset<M> bs(muveletek[j]);
            if((j%2 ==0 && kezdoMuvelet == XOR)||(j%2 == 1 && kezdoMuvelet == ADD)){
                left ^= bs;
            }else{
                short newLeft = left.to_ulong() + muveletek[j];
                bitset<M> seged(newLeft);
                left = seged;
            }
        }
        if(left != P.second)
            return false;
    }
    return true;
}

void solver(const short& N, const std::map<short, short> &charMap, vector<short>& muveletek, muveletTipus& kezdoMuvelet){
    while(muveletek.size() <= N) {
        kovetkezoLehetoseg(muveletek);
        if(joMegoldasE(charMap,muveletek,XOR)){
            kezdoMuvelet = XOR;
            return;
        }
        if(joMegoldasE(charMap,muveletek,ADD)) {
            kezdoMuvelet = ADD;
            return;
        }
    }
    cout << "Nincs megoldás, vagy el van rontva a program... :(\n";
}

void writeSolution(short N, const vector<short>& muveletek, const muveletTipus kezdoMuvelet){
    for(int i = 0; i < N; i++) {
        if(i < muveletek.size()){
            if((i%2 == 0 && kezdoMuvelet == XOR)||(i%2 == 1 && kezdoMuvelet == ADD))
                cout << "XOR";
            else
                cout << "ADD";
            cout << muveletek[i];
        }else
            cout << "ADD0";

        if(i != N-1)
            cout << endl;
    }
}

int main()
{
    short N;
    map<short, short> charMap;
    vector<short> muveletek;
    muveletTipus kezdomuvelet;

    readData(N, charMap);
    solver(N, charMap,muveletek,kezdomuvelet);
    writeSolution(N, muveletek, kezdomuvelet);

    //writeOutData(N, charMap);

    return 0;
}
