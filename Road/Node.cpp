//
// Created by dani on 3/20/20.
//

#include "Node.h"

Node::Node() {
    time = 0;
    parent = nullptr;
    city = build_city();
    children = {};
    road_pieces = {};
}

vector<vector<Field>> Node::build_city() {
    vector<vector<Field>> fields;
    for(int i = -radius; i <=  radius; ++i)
    {
        vector<Field> temp;
        for (int j = -radius; j <= radius; ++j) {
            Field field(i, j);
            if(abs(field.pos.u) + abs(field.pos.v) + abs(field.pos.w) > radius && !(i==0 && j==0))
            {
                field.value = -9;
            }
            else
            {
                field.set_value();
                field.set_possible_road();
                field.set_neighbors();
            }
            temp.push_back(field);
        }
        fields.push_back(temp);
    }
    return fields;
}

Node::Node(Node * parent_node, Field& road_field) : parent(parent_node), selected_road(road_field) {
    time = parent->time +1;
    city = parent_node->get_city();
    road_pieces = parent_node->road_pieces;
    children = {};
    Position p(road_field.pos.x, road_field.pos.y);
    this->insert_road_piece(p);
    for (auto nb : selected_road.neighbors)
    {
        city[nb.x + radius][nb.y + radius].road_done++;
    }
    city[selected_road.pos.x + radius][selected_road.pos.y + radius].value = -2;
}

void Node::city_heur() {
    float must = 0;
    float forbidden = 0;
    float outer_fields = 0;
    for (int i = 1; i <= radius; ++i)
    {
        outer_fields += i;
    }
    float map_size = pow((2*radius + 1),2) - 2*outer_fields;
    for (const auto& c_row : city)
    {
        for (const auto& field : c_row)
        {
            if (field.value == -2)
            {
                must++;
            }
            if (field.value == -3)
            {
                forbidden++;
            }
        }
    }
    score = map_size - numbered_fields.size() - must - forbidden;
}

list<Node *> Node::create_city_children() {
    set<Position> children_position;
    if (road_pieces.empty())
    {
        for (const auto& c_row : city)
        {
            for (const auto& field : c_row)
            {
                if (field.value == -1)
                {
                    Position p(field.pos.x, field.pos.y);
                    children_position.insert(p);
                }
            }
        }
    }
    for (const auto& rp : road_pieces)
    {
        for (const auto& nb : city[rp.front().x+radius][rp.front().y+radius].neighbors)
        {
            auto it = rp.begin();
            Position curr(it->x, it->y);
            it++;
            Position prev(it->x, it->y);
            if (city[nb.x +radius][nb.y +radius].value == -1 && !is_sharp_turn(prev, curr, nb))
            {
                children_position.insert(nb);
            }
        }
    }

    for (const auto& p : children_position)
    {
        Node *child = new Node(this, city[p.x + radius][p.y + radius]);
        children.push_back(child);
    }

    return children;
}

bool Node::is_sharp_turn(const Position &previous, const Position &current, const Position &next) {
    if (previous == next)
    {
        return true;
    }
    for (int i = 0; i < ordered_nbs.size(); ++i) {
        if (previous.x == current.x + ordered_nbs[i][0] && previous.y == current.y + ordered_nbs[i][1])
        {
            if (next.x == current.x + ordered_nbs[(i-1+ordered_nbs.size())%ordered_nbs.size()][0] && next.y == current.y + ordered_nbs[(i-1+ordered_nbs.size())%ordered_nbs.size()][1])
            {
                return true;
            }
            if (next.x == current.x + ordered_nbs[(i+1+ordered_nbs.size())%ordered_nbs.size()][0] && next.y == current.y + ordered_nbs[(i+1+ordered_nbs.size())%ordered_nbs.size()][1])
            {
                return true;
            }
        }
    }
    return false;
}

bool Node::check_if_city_finished() {
    for (const auto& num : numbered_fields)
    {
        if (city[num.pos.x + radius][num.pos.y + radius].road_done != city[num.pos.x + radius][num.pos.y + radius].value)
        {
            return false;
        }
    }
    if (road_pieces.size() != 1)
    {
        return false;
    } else{
        bool is_last = false;
        Position first = road_pieces.front().front();
        Position last = road_pieces.front().back();
        for(const auto& nb : city[last.x + radius][last.y +radius].neighbors) {
            if (nb == first) {
                is_last = true;
            }
        }
        if (!is_last)
        {
            return false;
        }
    }
    return true;
}

bool Node::allowed_field(const Position &p)
{
    Field field = city[p.x + radius][p.y + radius];
    for (const auto& nb : field.neighbors)
    {
        if (city[nb.x +radius][nb.y +radius].value >= 0)
        {
            if (city[nb.x +radius][nb.y +radius].road_done >= city[nb.x +radius][nb.y +radius].value)
            {
                return false;
            }
        }
    }
    return true;
}

bool Node::is_visited(const Position &p)
{
    Node * current_node = this;
    while (current_node != nullptr)
    {
        if (current_node->get_selected_road() == city[p.x +radius][p.y +radius])
        {
            return true;
        }
        current_node = current_node->get_parent();
    }
    return false;
}

bool Node::is_sharp_turn(const Position &p) {
    if (this->get_parent() != nullptr) {
        Field previous = parent->get_selected_road();
        Field current = selected_road;
        Field next = city[p.x + radius][p.y + radius];
        //cout << "prev: (" << previous.pos.x << "," << previous.pos.y << ") current: (" << current.pos.x << "," << current.pos.y << ") next: (" << next.pos.x << "," << next.pos.y << ")" << endl;
        if (previous == next)
        {
            return true;
        }
        for (int i = 0; i < ordered_nbs.size(); ++i) {
            if (previous.pos.x == current.pos.x + ordered_nbs[i][0] && previous.pos.y == current.pos.y + ordered_nbs[i][1])
            {
                if (next.pos.x == current.pos.x + ordered_nbs[(i-1+ordered_nbs.size())%ordered_nbs.size()][0] && next.pos.y == current.pos.y + ordered_nbs[(i-1+ordered_nbs.size())%ordered_nbs.size()][1])
                {
                    return true;
                }
                if (next.pos.x == current.pos.x + ordered_nbs[(i+1+ordered_nbs.size())%ordered_nbs.size()][0] && next.pos.y == current.pos.y + ordered_nbs[(i+1+ordered_nbs.size())%ordered_nbs.size()][1])
                {
                    return true;
                }
            }
        }
    }
    return false;
}

list<list<Position>> Node::get_road_pieces() {
    return road_pieces;
}

bool Node::analyze() {
    int gen = 0;
    while (true) {
        if (!this->check_if_possible() || this->is_closed()) //is_closed végig megy a cityn, és ha mustot talál, akkor ellenőrzi a szomszédait, hogy nincs-e bezárva
        {
            return false;
        }
        History h;
        h.city = city;
        h.road_pieces = road_pieces;
        h.gen = gen;
        history.push_back(h);

        set<Position> current_must = {};
        set<Position> current_forbidden = {};

        for (const auto &c_row : city) {
            for (const auto &field : c_row) {
                if (is_full(field))
                {
                    for (const auto& nb : field.neighbors)
                    {
                        if (city[nb.x + radius][nb.y + radius].value == -1)
                        {
                            current_forbidden.insert(nb);
                        }
                    }
                }
                if (this->is_stuck(field))
                {
                    Position p(field.pos.x, field.pos.y);
                    current_forbidden.insert(p);
                }

                if (is_pos_eq_val(field))
                {
                    for (const auto& nb : field.neighbors)
                    {
                        if (city[nb.x + radius][nb.y + radius].value == -1)
                        {
                            current_must.insert(nb);
                        }
                    }
                }

                Position p = this->is_directed(field);
                if (!(p.x == field.pos.x && p.y == field.pos.y))
                {
                    if (city[p.x + radius][p.y + radius].value == -1)
                    {
                        current_must.insert(p);
                    }
                }
            }
        }

        if(current_forbidden.empty() && current_must.empty())
        {
            return true;
        }
        else
        {
            this->modify_city(current_must, current_forbidden);
            gen++;
        }
    }
}


bool Node::check_if_possible()
{
    for (const auto& num : numbered_fields)
    {
        if (city[num.pos.x + radius][num.pos.y + radius].possible_road < city[num.pos.x + radius][num.pos.y + radius].value)
        {
            return false;
        }
    }
    return true;
}

bool Node::is_closed()
{
    for (const auto& c_row : city)
    {
        for (const auto& field : c_row)
        {
            if (field.value == -2)
            {
                if (field.possible_road == 1)
                {
                    return true;
                }
            }
        }
    }
    return false;
}

bool Node::is_full(const Field& field) {
    bool is_full = false;
    if (field.value >= 0)
    {
        if (field.value == field.road_done)
        {
            is_full = true;
        }
    }
    return is_full;
}

bool Node::is_stuck(const Field &field) {
    if (field.possible_road != 2 || field.value != -1)
    {
        return false;
    } else
    {
        for (int i = 0; i < ordered_nbs.size(); ++i)
        {
            Position nb_field (field.pos.x + ordered_nbs[i][0], field.pos.y + ordered_nbs[i][1]);
            Position nb_nb_field (field.pos.x + ordered_nbs[(i+1)%ordered_nbs.size()][0], field.pos.y + ordered_nbs[(i+1)%ordered_nbs.size()][1]);
            if (abs(nb_field.u) + abs(nb_field.v) + abs(nb_field.w) <= radius && abs(nb_nb_field.u) + abs(nb_nb_field.v) + abs(nb_nb_field.w) <= radius)
            {
                if ((city[nb_field.x + radius][nb_field.y + radius].value == -1 || city[nb_field.x + radius][nb_field.y + radius].value == -2) && (city[nb_nb_field.x + radius][nb_nb_field.y + radius].value == -1 || city[nb_nb_field.x + radius][nb_nb_field.y + radius].value == -2))
                {
                    return true;
                }
            }
        }
        return false;
    }
}

bool Node::is_pos_eq_val(const Field& field) {
    if (field.value >= 0)
    {
        if (field.possible_road == field.value)
        {
            return true;
        }
    }
    return false;
}

Position Node::is_directed(const Field &field) {
    Position p(field.pos.x, field.pos.y);
    if (field.value != -2)
    {
        return p;
    } else{
        for (int i = 0; i < ordered_nbs.size(); ++i)
        {
            Position nb_field (field.pos.x + ordered_nbs[i][0], field.pos.y + ordered_nbs[i][1]);
            if (abs(nb_field.u) + abs(nb_field.v) + abs(nb_field.w) <= radius)
            {
                if (city[nb_field.x + radius][nb_field.y + radius].value == -2)
                {
                    int count = 0;
                    Position out;
                    for (int j = 2; j <= 4; ++j)
                    {
                        Position nb_nb_field (field.pos.x + ordered_nbs[(i+j)%ordered_nbs.size()][0], field.pos.y + ordered_nbs[(i+j)%ordered_nbs.size()][1]);
                        if (abs(nb_nb_field.u) + abs(nb_nb_field.v) + abs(nb_nb_field.w) <= radius)
                        {
                            if (city[nb_nb_field.x + radius][nb_nb_field.y + radius].value == -2 || city[nb_nb_field.x + radius][nb_nb_field.y + radius].value == -1)
                            {
                                count++;
                                out = nb_nb_field;
                            }
                        }
                    }
                    if (count == 1)
                    {
                        return out;
                    }
                }
            }
        }
    }
    return p;
}

void Node::modify_city(const set<Position> &must, const set<Position> &forbidden) {
    for (auto m : must)
    {
        this->insert_road_piece(m);

        city[m.x + radius][m.y + radius].value = -2;
        for (auto nb : city[m.x + radius][m.y + radius].neighbors)
        {
            city[nb.x + radius][nb.y + radius].road_done++;
        }
    }
    for (auto f : forbidden)
    {
        city[f.x + radius][f.y + radius].value = -3;
        for (auto nb : city[f.x + radius][f.y + radius].neighbors)
        {
            city[nb.x + radius][nb.y + radius].possible_road--;
        }
    }
}

void Node::insert_road_piece(const Position &m) {
    if (!road_pieces.empty())
    {
        vector<list<list<Position>>::iterator> inserted;

        for (auto it = road_pieces.begin(); it != road_pieces.end(); ++it)
        {
            if (find(city[it->back().x+radius][it->back().y+radius].neighbors.begin(), city[it->back().x+radius][it->back().y+radius].neighbors.end(), m) != city[it->back().x+radius][it->back().y+radius].neighbors.end())
            {
                it->push_back(m);
                inserted.push_back(it);
            }
            else if (find(city[it->front().x+radius][it->front().y+radius].neighbors.begin(), city[it->front().x+radius][it->front().y+radius].neighbors.end(), m) != city[it->front().x+radius][it->front().y+radius].neighbors.end())
            {
                it->push_front(m);
                inserted.push_back(it);
            }
        }
        if (inserted.empty())
        {
            list<Position> new_rp;
            new_rp.push_back(m);
            road_pieces.push_back(new_rp);
        }
        if (inserted.size() == 2)
        {
            if (inserted[0]->front() == m && inserted[1]->front() == m)
            {
                inserted[0]->pop_front();
                inserted[0]->reverse();
                inserted[0]->insert(inserted[0]->end(), inserted[1]->begin(), inserted[1]->end());
                road_pieces.erase(inserted[1]);
            }
            if (inserted[0]->front() == m && inserted[1]->back() == m)
            {
                inserted[0]->pop_front();
                inserted[1]->insert(inserted[1]->end(), inserted[0]->begin(), inserted[0]->end());
                road_pieces.erase(inserted[0]);
            }
            if (inserted[0]->back() == m && inserted[1]->front() == m)
            {
                inserted[1]->pop_front();
                inserted[0]->insert(inserted[0]->end(), inserted[1]->begin(), inserted[1]->end());
                road_pieces.erase(inserted[1]);
            }
            if (inserted[0]->back() == m && inserted[1]->back() == m)
            {
                inserted[0]->pop_back();
                inserted[0]->reverse();
                inserted[1]->insert(inserted[1]->end(), inserted[0]->begin(), inserted[0]->end());
                road_pieces.erase(inserted[0]);
            }
        }
    } else{
        list<Position> new_rp;
        new_rp.push_back(m);
        road_pieces.push_back(new_rp);
    }
}

float Node::get_score() {
    return score;
}

vector<vector<Field>> Node::get_city() {
    return city;
}

Field Node::get_selected_road() {
    return selected_road;
}

int Node::get_time() {
    return time;
}

Node * Node::get_parent() {
    return parent;
}

list<Node *> Node::get_children() {
    return children;
}

void Node::dump() {
    cout << selected_road.pos.x << " " << selected_road.pos.y << endl;
}