//
// Created by nagyd on 2020. 04. 03..
//

#include "Position.h"

Position::Position() = default;

Position::Position(int x_, int y_) : x(x_), y(y_){
    transform_position();
}

Position::Position(int _u, int _v, int _w) : u(_u), v(_v), w(_w){
    x = _u + _w*(-1);
    y = _v + _w;

    cube_x = x;
    cube_z = y;
    cube_y = -cube_x - cube_z;

}

void Position::transform_position() {
    if (x > 0 && y < 0)
    {
        int m = fmin(abs(x),abs(y));
        u = x-m;
        v = y+m;
        w = -m;
    }
    else if(x < 0 &&  y > 0)
    {
        int m = fmin(abs(x),abs(y));
        u = x+m;
        v = y-m;
        w = m;
    }
    else
    {
        u = x;
        v = y;
        w = 0;
    }

    cube_x = x;
    cube_z = y;
    cube_y = -cube_x - cube_z;
}

bool Position::operator==(const Position &p) const
{
    return x == p.x && y == p.y;
}

bool Position::operator<(const Position &p) const {
    if (x < p.x)
    {
        return true;
    }
    else if (x == p.x)
    {
        return y < p.y;
    }
    else
    {
        return false;
    }
}

