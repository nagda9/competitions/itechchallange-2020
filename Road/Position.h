//
// Created by nagyd on 2020. 04. 03..
//

#ifndef PLANGTON_POSITION_H
#define PLANGTON_POSITION_H

#include "cmath"

using namespace std;

class Position {
public:
    int x{}, y{};
    int u{}, v{}, w{};
    int cube_x{}, cube_y{}, cube_z{};

    Position();
    Position(int _x, int _y);
    Position(int _u, int _v, int _w);
    void transform_position();
    bool operator== (const Position& p) const;
    bool operator< (const Position& p) const;
};


#endif //PLANGTON_POSITION_H
