////
//// Created by nagyd on 2020. 04. 02..
////
//
//#include "Draw.h"
//
//void Draw::draw_city(const vector<vector<Field>> &city) {
//    gout.open(W,H);
//    gout << color(255,255,255) << move_to (0, 0) << box(W,H);
//    stringstream ss;
//
//    for (const auto& c_row : city) {
//        for (const auto &field : c_row) {
//            if (field.value != -9)
//            {
//                hexagon(field);
//
//                double width = sqrt(3) * field.size;
//                double height = 2 * field.size;
//                double center_x, center_y;
//
//                center_x = origo_x + field.pos.x *width + field.pos.y*(width/2);
//                center_y = origo_y + field.pos.y *height*3/4;
//                string pos, done;
//                ss << field.possible_road;
//                ss >> pos;
//                ss.clear();
//                ss << field.road_done;
//                ss >> done;
//                ss.clear();
//                gout << color(0, 0, 200) << move_to(center_x - gout.twidth(pos + "/" + done)/2, center_y + gout.cascent()/2) << text(pos + '/' + done);
//            }
//        }
//    }
//
//    gout << refresh;
//    event ev;
//    while (gin >> ev && ev.keycode != key_escape)
//    {
//
//    }
//}
//
//void Draw::draw_analyzed(const vector<vector<Field>> &city, const list<list<Position>> &road_pieces) {
//    gout.open(W,H);
//    gout << color(255,255,255) << move_to (0, 0) << box(W,H);
//    stringstream ss;
//    double width = sqrt(3) * city[0][0].size;
//    double height = 2 * city[0][0].size;
//    double center_x, center_y;
//
//    for (const auto& c_row : city) {
//        for (const auto &field : c_row) {
//            if (field.value != -9)
//            {
//                hexagon(field);
//                width = sqrt(3) * field.size;
//                height = 2 * field.size;
//
//                center_x = origo_x + field.pos.x *width + field.pos.y*(width/2);
//                center_y = origo_y + field.pos.y *height*3/4;
//                string pos, done;
//                ss << field.possible_road;
//                ss >> pos;
//                ss.clear();
//                ss << field.road_done;
//                ss >> done;
//                ss.clear();
//                gout << color(0, 0, 200) << move_to(center_x - gout.twidth(pos + "/" + done)/2, center_y + gout.cascent()/2) << text(pos + '/' + done);
//
//                if (field.value == -2)
//                {
//                    gout << color(0, 255, 0) << move_to (center_x , center_y) << box(10,10);
//                }
//
//                if (field.value == -3)
//                {
//                    gout << color(255, 0, 0) << move_to (center_x , center_y) << box(10,10);
//                }
//            }
//        }
//    }
//
//    for (const auto& rp : road_pieces)
//    {
//        center_x = origo_x + rp.front().x *width + rp.front().y*(width/2);
//        center_y = origo_y + rp.front().y *height*3/4;
//        gout << color(0, 0, 0) << move_to (center_x , center_y) << box(5,5);
//        for(const auto& piece : rp)
//        {
//            center_x = origo_x + piece.x *width + piece.y*(width/2);
//            center_y = origo_y + piece.y *height*3/4;
//            gout << line_to (center_x , center_y);
//            if (piece == rp.back())
//            {
//                gout << box(5,5);
//            }
//        }
//    }
//
//    gout << refresh;
//    event ev;
//    while (gin >> ev && ev.keycode != key_escape)
//    {
//
//    }
//}
//
//void Draw::hexagon(Field field) {
//    gout << color(0,0,0) << move_to(origo_x, origo_y);
//    double width = sqrt(3) * field.size;
//    double height = 2 * field.size;
//    double center_x = origo_x + field.pos.x *width + field.pos.y*(width/2);
//    double center_y = origo_y + field.pos.y *height*3/4;
//
//    gout << color(0,0,0) << move_to(center_x, center_y);
//
//    for (int i = 0; i <= 6; ++i)
//    {
//        double angle_deg = 60 * i - 30;
//        double angle_rad = 3.14 / 180 * angle_deg;
//        double x = center_x + field.size * cos(angle_rad);
//        double y = center_y + field.size * sin(angle_rad);
//        if (i == 0)
//        {
//            gout << move_to(x, y);
//        } else{
//            gout << line_to(x, y);
//        }
//    }
//
//    stringstream ss;
//    string val;
//    if (field.value >= 0 || field.value == -9)
//    {
//        ss << field.value;
//        ss >> val;
//        gout << move_to(center_x - gout.twidth(val)/2, center_y - gout.cascent()/2) << text(val);
//    }
//}
