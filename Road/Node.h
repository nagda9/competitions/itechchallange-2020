//
// Created by dani on 3/20/20.
//

#ifndef PLANGTON_NODE_H
#define PLANGTON_NODE_H

#include <vector>
#include <set>
#include <array>
#include "Field.h"
//#include "Draw.h"

using namespace std;

struct History
{
    vector<vector<Field>> city;
    list<list<Position>> road_pieces;
    int gen;
};

/// Node-nak mindig a TELJES rendszer egy állapotát kell tekinteni!!! És ha így csináljuk, jól, akkor csak 1 parent lehet.
class Node {
private:
    vector<vector<Field>> city;
    Field selected_road;
    list<list<Position>> road_pieces;
    float score{};
    Node * parent{};
    list<Node *> children;
    int time;

public:
    vector<History> history;
    Node();
    Node(vector<vector<Field>> city, Field f);
    Node(Node * parent_node, Field& f);
    static vector<vector<Field>> build_city();

    void set_score(float s);
    float get_score();
    vector<vector<Field>> get_city();
    Field get_selected_road();
    int get_time();
    Node * get_parent();
    list<Node *> get_children();
    list<list<Position>> get_road_pieces();

    bool check_if_city_finished();

    bool check_if_finished();
    bool is_first();
    bool is_last();
    bool are_roads_done();
    void insert_road_piece(const Position &m);

    bool allowed_field(const Position &p);
    bool is_visited(const Position &p);
    bool is_sharp_turn(const Position &p);
    static bool is_sharp_turn(const Position &prev, const Position &current, const Position &next);

    list<Node *> create_children();
    list<Node *> create_city_children();

    void heur();
    void city_heur();

    void dump();

    bool analyze();
    bool check_if_possible();
    bool is_closed();
    static bool is_full(const Field& f);
    static bool is_pos_eq_val(const Field& field);
    bool is_stuck(const Field &field);
    Position is_directed(const Field& f);

    void modify_city(const set<Position> &must, const set<Position> &forbidden);

    static void dump_city (const set<Field>& d_city);
};


#endif //PLANGTON_NODE_H
