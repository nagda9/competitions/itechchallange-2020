//
// Created by dani on 3/20/20.
//

#ifndef PLANGTON_GRAPH_H
#define PLANGTON_GRAPH_H

#include "Node.h"
#include "Field.h"
#include <set>
#include <vector>
#include <list>
#include <cmath>
#include <iostream>
//#include <graphics.hpp>
//#include <Draw.h>

using namespace std;
//using namespace genv;

class Graph {
private:
    list<Node *> graph;
    list<Node **> open;
    vector<Node **> history;

public:
    Graph();
    list<Position> build_road();
    list<Node **>::iterator min_open();
    void dump_open();
};

vector<Field>::iterator find_field(Field& f, vector<Field>& v);

#endif //PLANGTON_GRAPH_H
