////
//// Created by nagyd on 2020. 04. 02..
////
//
//#ifndef PLANGTON_DRAW_H
//#define PLANGTON_DRAW_H
//
//#include "Field.h"
//#include <set>
//#include <cmath>
//
//#include "graphics.hpp"
//#include <sstream>
//
//
//using namespace std;
//using namespace genv;
//
//class Draw {
//private:
//    double W = 1024;
//    double H = 720;
//    double origo_x = W/2;
//    double origo_y = H/2;
//public:
//    //void draw(Node * node, int index);
//    void draw_city(const vector<vector<Field>> &city);
//    void draw_analyzed(const vector<vector<Field>> &city, const list<list<Position>> &road_pieces);
//    void hexagon(Field field);
//};
//
//
//#endif //PLANGTON_DRAW_H
