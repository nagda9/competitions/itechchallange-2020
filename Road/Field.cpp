//
// Created by dani on 3/20/20.
//

#include "Field.h"

Field::Field() = default;

Field::Field(int x_, int y_) {
    Position p(x_, y_);
    pos = p;
}

Field::Field(int u_, int v_, int w_) {
    Position p(u_, v_, w_);
    pos = p;
}

void Field::set_value() {
    for (auto& n : numbered_fields)
    {
        if (*this == n)
        {
            value = n.value;
        }
    }
}

void Field::set_possible_road() {
    for (int i = -1; i <= 1; i++) {
        for (int j = -1; j <= 1; j++) {
            if (!((i == -1 && j == -1) || (i == 1 && j == 1) || (i == 0 && j == 0))) // kizárom a négyzet sarkokat
            {
                Field f(this->pos.x + i, this->pos.y + j);
                if (abs(f.pos.u) + abs(f.pos.v) + abs(f.pos.w) <= radius) // megnézem, hogy a terület sugáron belül van-e
                {
                    if (find(numbered_fields.begin(), numbered_fields.end(), f) == numbered_fields.end()) // és hogy nem számozott-e
                    {
                        possible_road++;
                    }
                }
            }
        }
    }
}

void Field::set_neighbors() {
    for (int i = -1; i <= 1; i++) {
        for (int j = -1; j <= 1; j++) {
            if (!((i == -1 && j == -1) || (i == 1 && j == 1) || (i == 0 && j == 0)))
            {
                Position nb(this->pos.x +i, this->pos.y +j);
                if (abs(nb.u) + abs(nb.v) + abs(nb.w) <= radius)
                {
                    neighbors.push_back(nb);
                }
            }
        }
    }
}

bool Field::operator==(const Field& f) const
{
    return pos == f.pos;
}

bool Field::operator<(const Field &f) const {
    return pos < f.pos;
}



