#include <iostream>
#include <vector>
#include <array>
#include "Graph.h"

using namespace std;

int radius;
vector<Field> numbered_fields;
array<array<int,2>, 6> ordered_nbs = {{{-1, 1}, {-1, 0}, {0, -1}, {1, -1}, {1, 0}, {0, 1}}};

list<Position> find_route()
{
    Graph g;
    list<Position> road = g.build_road();
    return road;
    //return {};
}

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> radius >> n;
    numbered_fields.resize(n);
    for (auto& field : numbered_fields)
    {
        cin >> field.pos.x >> field.pos.y >> field.value;
        field.pos.transform_position();
    }

    auto&& route = find_route();
    cout << route.size() << '\n';
    for (auto& field : route)
        cout << field.x << " " << field.y << endl;
    return 0;
}