//
// Created by dani on 3/20/20.
//

#include "Graph.h"

Graph::Graph() {
    Node * parent_node = new Node();

    bool is_possible = parent_node->analyze(); //átalakítja a cityt, miközben ha körbezárt mustot talál, vagy is_possible = false, visszatér igazzal
    if (is_possible) {
        parent_node->city_heur(); // minél több a must, és minél kevesebb az undone, annál jobb pontot kell kapjon, és -1-et, ha kész van
        graph.push_back(parent_node);
        Node **o = new Node *(parent_node);
        open.push_back(o);
    }
}

list<Position> Graph::build_road() {
    while (!open.empty())
    {
        //dump_open();
        //cout << "------"<< endl;
        auto it = min_open();
        //cout << "SELECTED: ";
        //(**it)->dump();
        /*for (auto h : (**it)->history)
        {
            draw.draw_analyzed(h.city, h.road_pieces);
        }*/
        if ((**it)->check_if_city_finished())
        {
            list<Position> road = (**it)->get_road_pieces().front();
            return road;
        }

        if (!(**it)->create_city_children().empty()) {
            for (auto ch : (**it)->get_children()) {
                bool is_possible = ch->analyze();
                if (is_possible) {
                    ch->city_heur(); // minél több a must, és minél kevesebb az undone, annál jobb pontot kell kapjon, és -1-et, ha kész van
                    graph.push_back(ch);
                    Node **o = new Node *(ch);
                    open.push_back(o);
                }
            }
        }
        open.erase(it);
    }
    return {};
}

list<Node **>::iterator Graph::min_open()
{
    auto it_min = open.begin();

    for (auto it = open.begin(); it != open.end(); it++)
    {
        if((***it_min).get_score() > (***it).get_score())
        {
            it_min = it;
        }
    }
    return it_min;
}

void Graph::dump_open() {
    for (const auto o : open)
    {
        cout << (**o).get_selected_road().pos.x << " " << (**o).get_selected_road().pos.y << " score: "<< (**o).get_score() << endl;
    }
}

//void Graph::inspect() {
//
//    event ev;
//    int index = 0;
//    draw.draw(*history[index], index);
//    while (gin >> ev && ev.keycode != key_escape) {
//        if (ev.keycode == key_left) {
//            index = (index - 1 + history.size()) % history.size();
//            draw.draw(*history[index], index);
//        }
//        if (ev.keycode == key_right)
//        {
//            index = (index + 1 + history.size()) % history.size();
//            draw.draw(*history[index], index);
//        }
//        if (ev.keycode == key_down) {
//            index = (index - 5 + history.size()) % history.size();
//            draw.draw(*history[index], index);
//        }
//        if (ev.keycode == key_up)
//        {
//            index = (index + 5 + history.size()) % history.size();
//            draw.draw(*history[index], index);
//        }
//    }
//}
