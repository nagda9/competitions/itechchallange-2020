//
// Created by dani on 3/20/20.
//

#ifndef PLANGTON_FIELD_H
#define PLANGTON_FIELD_H

#include <list>
#include <iostream>
#include <set>
#include <vector>
#include <array>
#include <cmath>
#include <algorithm>
#include "Position.h"

using namespace std;

class Field {
public:

    Position pos;
    double size = 25;

    int value = -1;
    int possible_road = 0;
    int road_done = 0;
    vector<Position> neighbors; // Ez egy olyan paraméter, amit nehéz lenne minden cityben minden mezőn számon tartani, ezért mindig csak ott definiálom majd, ahol kell.


    Field();
    Field(int _x, int _y);
    Field(int _u, int _v, int _w);
    void set_value();
    void set_possible_road();
    void set_neighbors();

    bool operator == (const Field &f) const;
    bool operator<(const Field& f) const;
};

extern int radius;
extern vector<Field>numbered_fields;
extern array<array<int,2>, 6> ordered_nbs;

#endif //PLANGTON_FIELD_H
