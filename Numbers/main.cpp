#include <iostream>
#include <vector>
#include <cstdint>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

using namespace std;

unsigned int osszeg(vector<uint_fast32_t>& kockak, vector<int>& indexVector, uint_fast32_t N){
    unsigned int sumOfValues = 0;
    for(int i : indexVector)
        sumOfValues += kockak[i]%N;
    return (sumOfValues % N);
}

void koviIndexeles(vector<int>& indexVector, uint_fast32_t N){
    for(int i = 0; i < N; i++){
        if(indexVector[N-1-i] != N*2-1-i){
            indexVector[N-1-i]++;
            for(int j = 1; j <= i; j++){
                indexVector[N-1-i+j] = indexVector[N-1-i] + j;
            }
            return;
        }
    }
}

void indexKiiras(vector<int>& indexVector){
    for(int i : indexVector)
        cout << i << ", ";
    cout << endl;
}

vector<uint_fast32_t> select_cubes( vector<uint_fast32_t>&& kockak,  uint_fast32_t N){
    // TODO return 'count' count number from 'cubes'
    // whose sum can be divided by 'divisor' with zero remainder
    vector<uint_fast32_t > kivalasztottKockak(0);
    vector<int> indexVector(N);
    for(int i = 0; i < N; i++){
        indexVector[i] = i;
    }
    /*indexKiiras(indexVector);
    for(int i : kockak)
        cout << i << ", ";
    cout << endl;*/

    while(osszeg(kockak, indexVector, N) != 0){
        koviIndexeles(indexVector, N);
        //indexKiiras(indexVector);
    }

    if(osszeg(kockak, indexVector, N) == 0){
        for(int i : indexVector)
            kivalasztottKockak.push_back(kockak[i]);
    }

    return kivalasztottKockak;
}



int main()
{
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);
    std::uint_fast32_t boxA, boxB, boxC;
    std::cin >> boxA >> boxB >> boxC;
    auto N = boxA * boxB * boxC;
    std::vector<std::uint_fast32_t> cubes(N * 2);
    for (auto& cube : cubes) std::cin >> cube;
    if (auto&& result = select_cubes(std::move(cubes), N); result.size() == N)
        for (auto& cube : result) std::cout << cube << " ";
    else
        std::cout << "****";
    std::endl(std::cout);
}

/*
int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    uint_fast32_t boxA, boxB, boxC;
    vector<uint_fast32_t> cubes;
    srand (time(NULL));
    boxA = 10;
    boxB = 10;
    boxC = 10;
    for(unsigned int i =0; i< (2*boxA*boxB*boxC); i++){
        cubes.push_back(rand() % 1000);
    }
    //auto N =  boxA * boxB * boxC;
    auto N = 4;
    vector<uint_fast32_t> cubes2 = {3,3,3,2,2,2,1,1};

    if (auto && result = select_cubes(move(cubes2), N); result.size() == N)
        for (auto& cube : result) cout << cube << " ";
    else
        cout << "****";
    endl(cout);
}*/